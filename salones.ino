/*
  Truly easy access

  Es un firmware que permite leer el uid de las tarjetas rfid
  utilizando el modulo rfid-rc522 (spi) y un wemos D1, para enviar
  por mqtt el id,timestamp,cliente

  id = id de la tarjeta rfid
  timestamp = timestamp unix del evento que se registro
  cliente = el identificador del cliente

  conexiones

  RFID     ESP8266                Wemos D1 mini
  RST      GPIO05 (free GPIO)          D1
  SS       GPIO4 (free GPIO)           D2
  MOSI     GPIO13 (HW SPI)             D7
  MISO     GPIO12 (HW SPI)             D6
  SCK      GPIO14 (HW SPI)             D5
  GND      GND                         G
  3.3V  3.3V  3V3
*/




#include <SPI.h>
#include <MFRC522.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

#define cliente "salon1"
#define RST_PIN         D3           // Configurable, see typical pin layout above
#define SS_PIN          D8          // Configurable, see typical pin layout above
#define salida D2
#define breakfast 0x01
#define lunch 0x01
#define dinner 0x01
long offset =  (3600 * -6);

const char* ssid = "Xibalba Hackerspace";
const char* password = "IOT1234567";
const char* server = "192.168.1.2";
const char* updateTopic = "/ecsl/salones";
String csvData = "";
byte newData[4];
byte readCard[4];   // Stores scanned ID read from RFID Module
boolean msgSent = true;

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.
MFRC522::MIFARE_Key key;

WiFiClient espClient;
PubSubClient client(espClient);


WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, offset);

long lastReconnectAttempt = 0;

void callback(char* topic, byte* payload, unsigned int length) {
  // handle message arrived

  digitalWrite(salida, HIGH);
  delay(300);
  digitalWrite(salida, LOW);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}



boolean reconnect() {
  if (client.connect(cliente)) {
    Serial.println("conectandoMQTT");
    // Once connected, publish an announcement...
    client.publish("outTopic", "hello world");
    // ... and resubscribe
    client.subscribe("/ecsl/confirmacion");
  }
  return client.connected();
}


uint8_t getID() {

  // There are Mifare PICCs which have 4 byte or 7 byte UID care if you use 7 byte PICC
  // I think we should assume every PICC as they have 4 byte UID
  // Until we support 7 byte PICCs
  Serial.println(F("Scanned PICC's UID:"));
  for ( uint8_t i = 0; i < 4; i++) {  //
    readCard[i] = mfrc522.uid.uidByte[i];
    //   Serial.print(readCard[i], HEX);
  }
  //  Serial.println("");
  return 1;
}

void rfid() {
  // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
  if ( ! mfrc522.PICC_IsNewCardPresent())
    return;

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial())
    return;

  getID();
  // Show some details of the PICC (that is: the tag/card)
  csvData = readCard[0];
  csvData += readCard[1];
  csvData += readCard[2];
  csvData += readCard[3];
  csvData += ",";
  csvData += timeClient.getEpochTime();
  csvData += ",";
  csvData += cliente;
  Serial.println(csvData);
  // Halt PICC
  mfrc522.PICC_HaltA();
  // Stop encryption on PCD
  mfrc522.PCD_StopCrypto1();
  Serial.println("lectura terminada");
  msgSent = false;
}

/**
   Helper routine to dump a byte array as hex values to Serial.
*/
void dump_byte_array(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}


void setup() {
  Serial.begin(115200); // Initialize Serial communications with the PC
  pinMode(salida, OUTPUT);
  setup_wifi();
  //while (!Serial);    // Do nothing if no Serial port is opened (added for Arduinos based on ATMEGA32U4)
  SPI.begin();        // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522 card

  // Prepare the key (used both as key A and as key B)
  // using FFFFFFFFFFFFh which is the default at chip delivery from the factory
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
  timeClient.begin();
  client.setServer(server, 1883);
  client.setCallback(callback);

  lastReconnectAttempt = 0;
}

/**
   Main loop.
*/
void loop() {

  if (!client.connected()) {
    long now = millis();
    if (now - lastReconnectAttempt > 5000) {
      lastReconnectAttempt = now;
      // Attempt to reconnect
      if (reconnect()) {
        lastReconnectAttempt = 0;
      }
    }
  } else {
    // Client connected
    timeClient.update();
    rfid();
    client.loop();
    if (!msgSent) {
      char tempChar[csvData.length() + 1];
      csvData.toCharArray(tempChar, csvData.length() + 1);
      client.publish(updateTopic, tempChar);
      msgSent = true;
    }
  }


}
